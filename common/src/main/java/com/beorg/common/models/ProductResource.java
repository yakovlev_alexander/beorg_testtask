package com.beorg.common.models;

public class ProductResource {

    private String name;
    private double price;

    public ProductResource() {
    }

    public ProductResource(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
