package com.beorg.common.models;

public class AvgPriceResource {
    private double avg;

    public AvgPriceResource() {
    }

    public AvgPriceResource(double avg) {
        this.avg = avg;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }
}
