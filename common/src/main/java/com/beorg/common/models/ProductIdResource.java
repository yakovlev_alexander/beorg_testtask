package com.beorg.common.models;

public class ProductIdResource {
    private long productId;

    public ProductIdResource() {
    }

    public ProductIdResource(long productId) {
        this.productId = productId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
