package com.beorg.common.models;


public class PriceResource {
    private long productId;

    private double price;

    public PriceResource() {
    }

    public PriceResource(long productId, double price) {
        this.productId = productId;
        this.price = price;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
