package com.beorg.common;

import com.beorg.common.models.ProductResource;
import com.beorg.common.models.Status;
import feign.Headers;
import feign.RequestLine;

public interface ProductClient {
    @RequestLine("PUT")
    @Headers("Content-type: application/json")
    Status check(ProductResource productResource);
}
