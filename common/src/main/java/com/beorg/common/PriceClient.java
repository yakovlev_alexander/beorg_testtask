package com.beorg.common;

import com.beorg.common.models.AvgPriceResource;
import com.beorg.common.models.PriceResource;
import com.beorg.common.models.ProductIdResource;
import com.beorg.common.models.Status;
import feign.Headers;
import feign.RequestLine;

public interface PriceClient {
    @RequestLine("PUT")
    @Headers("Content-type: application/json")
    AvgPriceResource getAveragePrice(ProductIdResource productIdResource);

    @RequestLine("POST")
    @Headers("Content-type: application/json")
    Status addPrice(PriceResource priceResource);
}
