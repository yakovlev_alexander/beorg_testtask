package com.beorg.common;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;

public class ClientBuilder {
    protected static <T> T createClient(Class<T> type, String url) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(type, url);
    }

    public static ProductClient createProductClient() {
        return createClient(ProductClient.class, "http://localhost:8081/product");
    }

    public static PriceClient createPriceClient() {
        return createClient(PriceClient.class, "http://localhost:8082/price");
    }
}
