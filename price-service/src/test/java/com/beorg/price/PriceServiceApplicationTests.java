package com.beorg.price;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PriceServiceApplicationTests {

	@Autowired
	private PriceRepository repository;

	@Before
	public void setUp() throws Exception {
		repository.save(new Price(1, 11.1));
		repository.save(new Price(4, 22.2));
		repository.save(new Price(3, 33.3));
	}

	@After
	public void tearDown() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void setsIdOnSaveTest() {
		Price price3 = repository.save(new Price(3, 11.1));
		assertNotNull(price3.getId());
	}

	@Test
	public void findByProductIdTest() {
		Price price3 = new Price(3, 33.3);
		assertTrue(repository.findByProductId(3L).contains(price3));
	}

	@Test
	public void findAllSizeTest() {
		repository.save(new Price(3, 11.1));
		assertEquals(repository.findByProductId(3L).size(), 2);
	}
}
