package com.beorg.price;

import com.beorg.common.models.AvgPriceResource;
import com.beorg.common.models.PriceResource;
import com.beorg.common.models.ProductIdResource;
import com.beorg.common.models.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/price")
public class PriceController {

    @Autowired
    private PriceRepository priceRepository;

    @RequestMapping(method = RequestMethod.POST)
    public Status savePrice(@RequestBody PriceResource priceResource) {
        priceRepository.save(new Price(priceResource.getProductId(), priceResource.getPrice()));
        return Status.OK;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public AvgPriceResource getAvgPrice(@RequestBody ProductIdResource productIdResource) {
        List<Price> prices = priceRepository.findByProductId(productIdResource.getProductId());
        int size = prices.size();
        double avgPrice = 0;
        for (Price price : prices) {
            avgPrice += price.getPrice() / size;
        }
        return new AvgPriceResource(avgPrice);
    }
}
