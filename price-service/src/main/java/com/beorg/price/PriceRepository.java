package com.beorg.price;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PriceRepository extends MongoRepository<Price, Long> {
    List<Price> findByProductId(Long productId);
}
