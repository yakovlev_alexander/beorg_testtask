package com.beorg.api;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTests {

	@Autowired
	private ProductRepository repository;

	@Before
	public void setUp() throws Exception {
		repository.save(new Product("Product1"));
		repository.save(new Product("Product2"));
		repository.save(new Product("Product3"));
	}

	@After
	public void tearDown() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void setsIdOnSaveTest() throws Exception {
		Product product = repository.save(new Product("Soap"));
		assertNotEquals(product.getId(), 0);
	}

	@Test
	public void findByNameTest() throws Exception {
		Product product = new Product("Product1");
		assertEquals(repository.findByName(product.getName()), product);
	}
}
