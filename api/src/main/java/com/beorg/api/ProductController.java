package com.beorg.api;

import com.beorg.common.ClientBuilder;
import com.beorg.common.PriceClient;
import com.beorg.common.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Value("${price.threshold}")
    private double priceThreshold;

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.PUT)
    public Status checkProduct(@RequestBody ProductResource productResource) {
        String name = productResource.getName();
        double price = productResource.getPrice();
        Product product = productRepository.findByName(name);
        if (product == null) {
            return addNewProduct(name, price);
        } else {
            return checkAvgPrice(price, product);
        }
    }

    private Status checkAvgPrice(double price, Product product) {
        PriceClient priceClient = ClientBuilder.createPriceClient();
        AvgPriceResource avgPriceResource = priceClient.getAveragePrice(new ProductIdResource(product.getId()));
        double avgPrice = avgPriceResource.getAvg();
        if (price > (1 - priceThreshold) * avgPrice && price < (1 + priceThreshold) * avgPrice) {
            return addPrice(product.getId(), price);
        } else {
            return Status.ERROR;
        }
    }

    private Status addNewProduct(String name, double price) {
        Product newProduct = productRepository.save(new Product(name));
        return addPrice(newProduct.getId(), price);
    }

    private Status addPrice(long productId, double price) {
        PriceClient priceClient = ClientBuilder.createPriceClient();
        return priceClient.addPrice(new PriceResource(productId, price));
    }
}
