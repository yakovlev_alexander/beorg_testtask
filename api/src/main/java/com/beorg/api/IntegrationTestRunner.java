package com.beorg.api;

import com.beorg.common.ClientBuilder;
import com.beorg.common.ProductClient;
import com.beorg.common.models.ProductResource;

/**
 * Run test when API and Price web services running
 */
public class IntegrationTestRunner {

    public static void main(String[] args) {
        ProductClient productClient = ClientBuilder.createProductClient();

        System.out.println("Adding 3 different product");
        productClient.check(new ProductResource("Sugar", 10));
        productClient.check(new ProductResource("Soap", 20));
        productClient.check(new ProductResource("Cheese", 30));

        System.out.println("Check Sugar price 2.9 (ERROR): " + productClient.check(new ProductResource("Sugar", 2.9)));
        System.out.println("Check Sugar price 3.1 (OK): " + productClient.check(new ProductResource("Sugar", 3.1)));
        System.out.println("Check new AVG Sugar price 2.9 (OK): " + productClient.check(new ProductResource("Sugar", 2.9)));

    }
}
